# Kane's Next.js Boilerplate
## Starting a project
```bash
mkdir my-project
cd my-project
git init
npm init
npm i https://gitlab.com/kane-c/next-boilerplate/-/archive/master/next-boilerplate-master.tar.gz
```

## Goodies included
* TypeScript
* Bootstrap 4 + Reactstrap + Sass
* Jest
* [Airbnb + TypeScript ESLint config](https://gitlab.com/kane-c/eslint-config)
* FontAwesome icons
* Sentry
* Next SEO
* Google Analytics types

## Future plans
* AMP
* Offline support
* GraphQL with Apollo + Postgraphile
